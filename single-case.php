<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="single-case padding--both">
    <div class="single-case__container">
      <div class="single-case__row">

        <article class="single-case__intro" itemscope itemtype="http://schema.org/BlogPosting">

          <div itemprop="articleBody">
            <?php the_content(); ?>
          </div>

        </article>

      </div>
    </div>
  </section>

  <?php 
    if (have_rows('case_results') ) : 
  ?>
  <section class="single-case padding--bottom">
    <div class="wrap hpad">
      <div class="row">

        <div class="single-case__results">
          <h2 class="single-case__title">Resultater</h2>

          <div class="single-case__row">

            <?php 
                while (have_rows('case_results') ) : the_row();  

                $result = get_sub_field('case_result');
                $result_text = get_sub_field('case_result_text');
            ?>

              <div class="single-case__result">
                <h2 class="single-case__title green"><?php echo esc_html($result); ?></h2>
                <?php echo esc_html($result_text); ?>
              </div>
            
            <?php endwhile; ?>

          </div>

        </div>

      </div>
    </div>
  </section>
  <?php endif; ?>

  <?php 
    $mockup = get_field('case_mockup'); 

    if ($mockup) :
  ?>

  <section class="single-case">
    <div class="wrap hpad">
      <div class="row--fluid">
        <div class="single-case__browser-header">
          <div class="single-case__browser-controls">
            <span class="bg--red"></span>
            <span class="bg--yellow"></span>
            <span class="bg--green"></span>
          </div>
        </div>


        <div class="single-case__browser-wrap">
          <div class="row single-case__browser-row flex flex--wrap">

            <div class="col-sm-12">

                <img src="<?php echo esc_url($mockup['url']); ?>" alt="">

            </div>

          </div>
        </div>
            
      </div>

    </div>
  </section>
  <?php endif; ?>

  <?php get_template_part('parts/cases'); ?>

  <?php get_template_part('parts/contact'); ?>

</main>

<?php get_template_part('parts/footer'); ?>