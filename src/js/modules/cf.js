import { ConversationalForm } from 'conversational-form';


function cfInit() {

  var ga_category = 'CF form - myQuestionForm';

  const cfInstance = new ConversationalForm({
    formEl: document.getElementById('form'),
    context: document.getElementById('cf-context'),
    robotImage: '/wp-content/uploads/2020/10/emoji_christobal.png',
    userImage: '/wp-content/uploads/2020/11/user-solid.svg', 
    loadExternalStyleSheet: true,
    preventAutoFocus: false,
    userInterfaceOptions: {
        controlElementsInAnimationDelay: 150,
        robot: {
            robotResponseTime: 800,
            showThinking: true
        },
        user: {
            showThinking: false
        }
    },
    dictionaryData: { 
      'entry-not-found': 'Oversættelsen blev ikke fundet.',
      'input-placeholder': 'Indtast dit svar her ...',
      'input-placeholder-error': 'Forkert input, prøv igen ...',
      'input-placeholder-required': 'Dette felt er påkrævet ...',
      'input-placeholder-file-error': 'Upload af fil mislykkedes ...',
      'input-placeholder-file-size-error': 'Filstørrelsen er for stor ...',
      'input-no-filter': 'Ingen resultater for {input-value} ...',
      'group-placeholder': 'Skriv for at filtrere ...',
      'user-reponse-and': ' og ',
      'user-reponse-missing': 'Manglende input ...',
      'user-reponse-missing-group': 'Intet valgt ...',
      'user-image': 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxjaXJjbGUgY3g9IjEwMCIgY3k9IjEwMCIgcj0iMTAwIiBmaWxsPSIjMzAzMDMwIi8+CjxwYXRoIGQ9Ik0xMDAgNTVMMTM4Ljk3MSAxMjIuNUg2MS4wMjg5TDEwMCA1NVoiIGZpbGw9IiNFNUU2RUEiLz4KPC9zdmc+Cg==',
      'icon-type-file': "<svg class='cf-icon-file' viewBox='0 0 10 14' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'><g stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'><g transform='translate(-756.000000, -549.000000)' fill='#0D83FF'><g transform='translate(736.000000, 127.000000)'><g transform='translate(0.000000, 406.000000)'><polygon points='20 16 26.0030799 16 30 19.99994 30 30 20 30'></polygon></g></g></g></g></svg>",
      'awaiting-mic-permission': 'Afventer tilladelse til mikrofon',
      'microphone-terminal-error': 'Lydindgang understøttes ikke',
      'user-audio-reponse-invalid': "Jeg forstod det ikke, prøv igen.",
      'general': 'Generel Type1|Generel Type2',
    },
    dictionaryRobot: {
      'robot-image': '/wp-content/uploads/2020/10/emoji_christobal.png',
      'text': 'Skriv venligst noget tekst.',
      'textarea': 'Skriv venligst noget tekst..',
      'input': 'Skriv venligst lidt tekst.',
      'name': 'Hvad hedder du?',
      'email': 'Din e-mail',
      'password': 'Indtast dit password',
      'tel': 'Hvad er dit telefonnummer?',
      'radio': 'Jeg har brug for, at du vælger en af disse.',
      'checkbox': 'Vælg så mange, som du vil.',
      'select': 'Vælg en af disse muligheder.',
      'file': 'Vælg en fil, der skal uploades.',
      'general': 'Generelt1|Generelt2|Generelt3..',
    },
    flowStepCallback: function(dto, success, error) {
      var currentStep = cfInstance.flowManager.getStep() + 1; // Steps are 0-based so we add 1
      var maxSteps = cfInstance.flowManager.maxSteps; // This value is not 0-based
      var ga_action = "Step " + currentStep + "/" + maxSteps;
      var ga_label = "Field name - " + dto.tag.name; // We only track actual field name for reference purpose. If you want to track the actual value you may do so.


      dataLayer.push({
          'event' : 'formSubmitted', 
          'formName' : 'Chatbot'
        });  


      success();
    },  
    submitCallback: function() {
        const scriptURL = 'https://usebasin.com/f/76131f67636b.json'
        cfInstance.addRobotChatResponse("Det var alt for nu. Tak for din interesse ❤️  Vi vender tilbage til dig hurtigst muligt. Har du spørgsmål kan du henvende dig på info@lionlab.dk eller +45 60 56 34 95");
        fetch(scriptURL, { method: 'POST', body: new FormData(form), dataType: 'json'});

        dataLayer.push({
          'event' : 'formSubmitted', 
          'formName' : 'Chatbot'
        });
    }
  });
}

export {cfInit}