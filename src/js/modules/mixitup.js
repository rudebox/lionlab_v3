import mixitup from 'mixitup';

function mixit() {

	const mixitContainer = document.querySelectorAll('.mixit');

	if (mixitContainer.length > 0) {

		const mixer = mixitup('.mixit', {
			animation: {
				easing: 'cubic-bezier(.75, -0.5, 0, .75)'
			}
		});
	}
}

mixit();

export {mixit}