import { gsap, TweenMax } from "gsap";
import { cfInit } from "./cf";

const tl = gsap.timeline();
const modal = document.querySelector('.modal');
const modalClose = document.querySelector('.modal__close');
const overlay = document.querySelector('.overlay');
const modalToggle = document.querySelector('.nav__modal-trigger');
const navToggle = document.querySelector('.nav-toggle');
const flaskBg = document.querySelectorAll('.header__logo--flask-bg');
const flask = document.querySelectorAll('.header__logo--flask-2');
const links = document.querySelectorAll('.nav__link');

function toggleModal () {

  tl.set(modal, {
    transform: 'translateY(100vh)'
  }); 

  tl.fromTo(modal, {transform: 'translateY(100vh)'}, {
        duration: .4,
        transform: 'translateY(0)'
  }).reversed(true);

  function toggleDirection() {
    tl.reversed(!tl.reversed());
  }

  modalToggle.onclick = function () { 
    cfInit(); 
  	toggleDirection();
  	overlay.classList.toggle('is-visible');
  	document.querySelector('body').classList.toggle('is-fixed');
    document.querySelector('.nav__dropdown').classList.remove('is-visible');
    document.querySelector('.nav__dropdown-trigger, .nav__notifications').classList.toggle('is-disabled');
    document.querySelector('.header__logo').classList.toggle('is-disabled');
    navToggle.classList.toggle('is-disabled');
    links.forEach(element => {
      element.classList.toggle('is-disabled');
    });
  };

  modalClose.onclick = function () {   
  	document.querySelector('body').classList.remove('is-fixed');
  	overlay.classList.remove('is-visible');
    document.querySelector('.nav__dropdown-trigger, .nav__notifications').classList.remove('is-disabled');
    document.querySelector('.header__logo').classList.remove('is-disabled');
    navToggle.classList.remove('is-disabled');
    links.forEach(element => {
      element.classList.remove('is-disabled');
    });
	  tl.reverse();
  };
}

toggleModal();