const trigger = document.querySelector('.nav__dropdown-trigger');
const dropdown = document.querySelector('.nav__dropdown');
const overlay = document.querySelector('.overlay');
const logo = document.querySelector('.header__logo');
const notifications = document.querySelector('.blog__notifications-count');

trigger.onclick = function () { 
	dropdown.classList.toggle('is-visible');
	overlay.classList.toggle('is-visible');
	document.querySelector('.nav__modal-trigger').classList.toggle('is-disabled');
	logo.classList.toggle('is-disabled');
	notifications.style.display = 'none';
};