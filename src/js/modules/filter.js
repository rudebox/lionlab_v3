import { gsap, Elastic } from "gsap";

function toggleFilters () {

  const tl = gsap.timeline();
  const trigger = document.querySelector('.blog__filter--dropdown');
  let filters = document.querySelectorAll('.blog__filter');

  if (filters.length > 0) {

    tl.set(filters, {
      opacity: 0,
      transform: 'translateY(-200px)'
    }); 

    tl.fromTo(filters, {opacity: 0, transform: 'translateY(-200px)'}, {
          duration: .8,
          opacity: 1,
          stagger: .1,
          ease: Elastic.easeOut.config(.75, -0.5, 0, .75),
          transform: 'translateY(0px)'
    }).reversed(true);

    function toggleDirection() {
      tl.reversed(!tl.reversed());
    }

    trigger.onclick = function () { 
      toggleDirection();
      trigger.classList.toggle('is-active');
    };
  }
}

toggleFilters();

export {toggleFilters}