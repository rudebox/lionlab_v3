import barba from '@barba/core'; 
import {gsap, TweenMax, Elastic} from "gsap";
import {grid } from './masonry';
import {toggleFilters} from './filter';
import {accordion} from './accordion';
import {lightbox} from './video-lightbox';
import {animateAvatars} from './hover-animate';
import {toggleMenu} from './menu';
import {mixit} from './mixitup'; 


const tl3 = gsap.timeline();  

const tl2 = gsap.timeline();

const meatCurtain = document.querySelector('.transition__curtain');

const tl = gsap.timeline();

const logo = document.querySelector('.header__logo');
const flasks = document.querySelectorAll('.header__logo--flask, .header__logo--flask-2');
const logoEl = document.querySelectorAll('.header__logo--head, .header__logo--flask-bg, .header__logo--flask, .header__logo--flask-2');
const letter = document.querySelector('.header__logo--letter');

let btns = document.querySelectorAll('.header__logo, .blog__notifications-btn');

const indicator = document.querySelector('.nav__line');

const navLink = document.querySelector('.nav__link');

const body = document.querySelector('body');

let links = document.querySelectorAll('.nav__link');
let navItems = document.querySelectorAll('.nav__item');


function loader(data) {

  if (document.querySelector('body').classList.contains('home') ) {

    const tl4 = gsap.timeline({

      onStart: function () {
        document.querySelector('html').removeAttribute('style');
      },

      onComplete: function() {
        body.classList.remove('is-fixed');
        document.querySelector('html').removeAttribute('class');
        document.querySelector('.header__logo').removeAttribute('style');
      }
    });


    document.querySelector('html').classList.add('is-loading');
    body.classList.add('is-fixed');
      

    tl4.set(logo, {     
      left: '50%',
      top: '50%',
      x: '-50%',
      y: '-50%',
      zIndex: 99999999,
      transition: 'all ease .6s',
      position: 'fixed'
    });

    tl4.to(logo, {      
      left: 0,
      top: 0,
      x: '0%',
      y: '0%',
      delay: 3.4,
      position: 'relative'
    });
    
    tl4.fromTo(logoEl, {opacity: 1}, {
        duration: .3,
        opacity: 1
    });

    TweenMax.fromTo(flasks, {rotation: 0}, {
        repeat: 2,
        duration: 1.4,
        rotation: 360,
        ease: Elastic.easeOut.config(.75, -0.5, 0, .75),
        svgOrigin: '515px 87px',
        transformOrigin: '515px 87px'
    });


    tl3.set(flasks, { 
      svgOrigin: '515px 87px',
      transformOrigin: '515px 87px'
    });

    
    tl2.set(meatCurtain, {
      ease: Elastic.easeOut.config(1, 0, 0, 1),
        height: '100vh',
        top: 0,
        overflow: 'hidden',
        transform: 'translateY(0vh) translateZ(0)',
        left: 0,
        bottom: 0,
        zIndex: 9999999,
        right: 0,
        position: 'fixed',
    })
    
    
    .to(meatCurtain, {
          transform: 'translateY(-100vh) translateZ(0)',
          duration: .4,
          delay: 4
    });

    TweenMax.fromTo('.nav__menu, .nav-toggle', {opacity: 0}, {
        opacity: 1,
        delay: 4,
        onComplete: function() {
          document.querySelectorAll('.nav__menu').forEach(menu => menu.removeAttribute('style'));
          document.querySelector('.nav-toggle').removeAttribute('style');
        }
    });
  }
}

function loaderIn(trigger, data) {

  tl2.set(meatCurtain, {
    ease: Elastic.easeOut.config(1, 0, 0, 1),
      height: '100vh',
      top: 0,
      transform: 'translateY(100vh) translateZ(0)',
      left: 0,
      bottom: 0,
      zIndex: 9999999,
      right: 0,
      position: 'fixed'
  });
  
  if (window.innerWidth < 768 && body.classList.contains('is-open') )  {     

    tl2.to(meatCurtain, {
      transform: 'translateY(0vh) translateZ(0)',
      duration: .4,
      delay: .6,
      onComplete: function() {

        window.scroll({
          top: 0,
          left: 0,
          behavior: 'smooth'
        });
      }
    });
  } 
  else {

   tl2.to(meatCurtain, {
      transform: 'translateY(0vh) translateZ(0)',
      duration: .4,
      onComplete: function() {

        window.scroll({
          top: 0,
          left: 0,
          behavior: 'smooth'
        });
      }
    });
  }
}

function pageTransition(next, data) {

  tl2.to(meatCurtain, {
    transform: 'translateY(-100vh) translateZ(0)',
    delay: .8,
    duration: .4
  });

  tl.fromTo(logoEl, {opacity: 1}, {
      duration: 1.2,
      opacity: 1
  });

  TweenMax.fromTo(flasks, {rotation: 0}, {
      duration: 1.4,
      rotation: 360,
      delay: .4,
      ease: Elastic.easeOut.config(.75, -0.5, 0, .75),
      svgOrigin: '515px 87px',
      transformOrigin: '515px 87px'
  });

  // tl.fromTo(letter, {x: -340}, { 
  //     duration: .8,
  //     x: 0
  // }, '-=2.4');

  // tl.set(letter, { 
  //   x: -340
  // });

  // tl.set(logoEl, { 
  //   opacity: 0
  // });

  tl.set(flasks, { 
    // opacity: 0,
    svgOrigin: '515px 87px',
    transformOrigin: '515px 87px'
  });

  return tl;
}

//Dr. Hooks
barba.hooks.afterOnce((data) => {

});

barba.hooks.before((data) => {
  document.querySelector('html').classList.add('is-loading');
  document.querySelector('.header').classList.add('is-fixed');
  document.querySelector('.nav__dropdown').classList.remove('is-visible');
  document.querySelector('.overlay').classList.remove('is-visible');
  document.querySelector('.nav__modal-trigger').classList.remove('is-disabled');  
});

barba.hooks.after((data) => {
  document.querySelector('html').removeAttribute('class');
  document.querySelector('.header').classList.remove('is-fixed');

  ga('set', 'page', window.location.pathname);
  ga('send', 'pageview');
});

barba.hooks.afterEnter((data) => {

});


barba.hooks.enter((data) => {

  links.forEach(link => {
    if (link.href === data.next.url.href) {
        link.classList.add('is-active');
    } else {
        link.classList.remove('is-active');
    }

    if (link.href === window.location.href) {
      link.classList.add('is-active');
    } else {
      link.classList.remove('is-active');
    }
  });

  if (window.innerWidth > 768 ) { 
    btns.forEach((btn) => { 
      btn.onclick = function () {
        indicator.classList.remove('is-active');
      };
    }); 

    let btnsCases = document.querySelectorAll('.cases__item, .blog__posts, .services-columns__column, .contact__btn');

    btnsCases.forEach((btnCase) => {
      btnCase.onclick = function () {
        indicator.classList.remove('is-active');
      };
    }); 
  }

  grid();
  toggleFilters();
  accordion();
  lightbox();
  animateAvatars();
  mixit();

  if (window.innerWidth < 768 ) { 
    toggleMenu();
  }
});

barba.hooks.leave(({current, data}) => {
  document.querySelector('.overlay').classList.remove('is-visible');
  document.querySelector('.nav--mobile').classList.remove('is-open');
  logo.classList.remove('is-disabled');

  navItems.forEach(navItem => {
    navItem.classList.remove('is-active');
  });   

});

barba.hooks.afterEnter(({current, data}) => {
  
});


barba.init({
  debug: false,

  schema: {
    prefix: 'data-app'
  },

  transitions: [{
    sync: true,

    beforeOnce (data) {
      
    },

    once (data) {
      loader();
    },

    leave({data, current}) {
        loaderIn();
    },

    async enter({data, next, current}) {
      document.body.classList = next.html.match(/<body class="(.*?)" itemscope itemtype="(.*?)" data-app="(.*?)">/)[1];
      await pageTransition();
    },
  }]
});