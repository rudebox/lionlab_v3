//toggle accordion  
import {TweenMax, Elastic, Expo} from "gsap";


function accordion () {
	class Accordion {
	  constructor() {
	    this.items = document.querySelectorAll(".accordion__wrapper");
	    this.itemClass = ".accordion__wrapper";
	    this.contentWrapperClass = ".accordion__panel";
	    this.css = {
	      open: "is-active"
	    };

	    if (this.items.length > 0) {
	      this.init();
	    }
	  }

	  init() {
	    for (let i = 0; i < this.items.length; i++) {
	      this.items[i].addEventListener("click", ev => {
	        ev.preventDefault();
	        const current = ev.currentTarget;
	        const contentWrapper = current.querySelector(this.contentWrapperClass);

	        if (!current.classList.contains(this.css.open)) {
	          this.slideDown(current, contentWrapper);
	          return;
	        }

	        this.closeItem();
	      });
	    }
	  }

	  getActiveElement() {
	    const accordionItems = document.querySelectorAll(`${this.itemClass}`);
	    let active = null;

	    for (let i = 0; i < accordionItems.length; i++) {
	      if (accordionItems[i].classList.contains(this.css.open)) {
	        active = accordionItems[i];
	      }
	    }
	    return active;
	  }

	  slideDown(element, content) {
	    const contentHeight = 0;
	    const active = this.getActiveElement();

	    if (active) {
	      const activeContent = active.querySelector(this.contentWrapperClass);
	      const height = activeContent.offsetHeight;

	      for (let i = 0; i < this.items.length; i++) {
	        this.items[i].classList.remove(this.css.open);
	      }

	      element.classList.add(this.css.open);

	      TweenMax.set(activeContent, {
	        height: height,
	        onComplete: () => {
	          active.classList.remove(this.css.open);

	          TweenMax.to(activeContent, 0.6, {
	            height: 0,
	            ease: Expo.easeOut,
	            onStart: () => {
	              this.openItem(content, contentHeight);
	            }
	          });
	        }
	      });
	      return;
	    }
	    // else
	    for (let i = 0; i < this.items.length; i++) {
	      this.items[i].classList.remove(this.css.open);
	    }

	    element.classList.add(this.css.open);

	    this.openItem(content, contentHeight);
	  }

	  openItem(content, contentHeight) {
	    TweenMax.set(content, {
	      height: "auto",
	      onComplete: () => {
	        contentHeight = content.clientHeight;
	        TweenMax.set(content, {
	          height: 0,
	          onComplete: () => {
	            TweenMax.to(content, 0.6, {
	              height: contentHeight,
	              ease: Elastic.easeOut.config(1, 0.5),
	              onComplete: () => {
	                TweenMax.set(content, {
	                  height: "auto",
	                  ease: Expo.easeOut,
	                });
	              }
	            });
	          }
	        });
	      }
	    });
	  }

	  closeItem() {
	    const active = this.getActiveElement();

	    if (active) {
	      const activeContent = active.querySelector(this.contentWrapperClass);

	      TweenMax.set(activeContent, {
	        height: activeContent.offsetHeight
	      });

	      active.classList.remove(this.css.open);

	      TweenMax.to(activeContent, 0.6, {
	        height: 0,
	        ease: Expo.easeOut,
	      });
	    }
	  }
	}

	new Accordion();
}

accordion();

export {accordion}
