import { gsap, TweenMax, Elastic } from "gsap";

function toggleMenu () {
	const tl = gsap.timeline();

	const menuToggle = document.querySelector('.nav-toggle');
	const mobileMenu = document.querySelector('.nav--mobile');
	const overlay = document.querySelector('.overlay');
	const logo = document.querySelector('.header__logo');
	const body = document.querySelector('body');
	const navItem = document.querySelectorAll('.nav__menu--main > .nav__item');

	tl.set(navItem, {
		transform: 'translateY(30px)',
		opacity: 0
	}); 

	tl.fromTo(navItem, {transform: 'translateY(30px)', opacity: 0}, {
	    duration: .6,
	    delay: .6,
	    stagger: .1,
	    opacity: 1,
	    ease: Elastic.easeOut.config(.75, -0.5, 0, .75),
	    transform: 'translateY(0)',
	}).reversed(true);

	function toggleDirection() {
		tl.reversed(!tl.reversed());
	}

	menuToggle.onclick = function () { 
		toggleDirection();
		event.preventDefault();
		mobileMenu.classList.toggle('is-open');
		body.classList.toggle('is-open');
		body.classList.toggle('is-fixed');
		overlay.classList.toggle('is-visible');
		logo.classList.toggle('is-disabled');
	};			
}


if (window.innerWidth < 767) {
	toggleMenu();
}

export {toggleMenu}