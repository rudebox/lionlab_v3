import {TweenMax, Elastic} from "gsap";

function animateAvatars() {

	if (document.querySelectorAll('.accordion__title').length > 0) {

		let acc = document.querySelectorAll('.accordion__title');
		let imgs = document.querySelectorAll('.services__employees-img');

		TweenMax.set(imgs, {scale: 0, autoAlpha: 0}, {});

		//set first element to active
		const activeElement = imgs[0];

		TweenMax.set(activeElement, {scale: 1, autoAlpha: 1}, {});

		acc.forEach.call(acc, function(el) {

			el.addEventListener('click', function(e) {

				imgs.forEach(img => {

					const imgAttr = img.getAttribute('data-index');

					if (this.getAttribute('data-index') === imgAttr) {

						TweenMax.to(img, .6, {
					        scale: 1,
					        autoAlpha: 1,
					        ease: Elastic.easeOut.config(.75, -0.5, 0, .75)
			      		});
					} else {

						TweenMax.to(img, .3, {
					        scale: 0,
					        autoAlpha: 0,
					        ease: Elastic.easeOut.config(.75, -0.5, 0, .75)
			      		});
					}
				});	
			});
		});

	}
}

animateAvatars();

export {animateAvatars}