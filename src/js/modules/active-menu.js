function activeMenu () {
  let menu = document.getElementById('menu-main-nav');

  menu.insertAdjacentHTML('beforeend', '<li class="nav__line"></li>');

  const indicator = document.querySelector('.nav__line');
  const items = document.querySelectorAll('.nav__link');

  function handleIndicator(el) {
    items.forEach(item => {
      // item.classList.remove('is-active');
    });

    indicator.style.width = `${el.offsetWidth}px`;
    indicator.style.left = `${el.offsetLeft}px`;
    indicator.classList.add('is-active');

    // el.classList.add('is-active');
  }


  items.forEach((item, index) => {
    item.addEventListener('click', (e) => { handleIndicator(e.target)});
    item.classList.contains('is-active') && handleIndicator(item);
  }); 

}

if (window.innerWidth > 767) {
  activeMenu();
}

export {activeMenu}