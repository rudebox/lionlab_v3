import Masonry from 'masonry-layout'

function grid() {

	const gridMs = document.querySelectorAll('.masonry');

	if (gridMs.length > 0) {
		const masonry = new Masonry(gridMs[0]);
	}
}

grid();

export {grid}