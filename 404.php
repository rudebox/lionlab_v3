<?php get_template_part('parts/header'); ?>

<main>

	<section class="page__hero">

		<div class="page__container">
			<div class="row">

				<div class="page__col">
					<h1 class="page__title">		
						<span class="page__slug">404</span>			
						Siden kunne ikke findes
					</h1>
					<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/lion.svg'); ?>

				</div>

			</div>
		</div>
	</section>

  <section class="error">
  	<div class="error__container">
  		<div class="error__row">

			<div class="error__col bg--yellow">
				<span class="error__icon">🤷‍♀️</span>
			</div>

			<div class="error__col">
				<h2>404</h2>
    			<p>Beklager, men vi kunne finde den side du søgte</p>    			
    			<a class="error__btn" href="/">Til forsiden</a>
			</div>

    	</div>
    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>