<?php

if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

require_once('includes/scratch.php');
require_once('lib/theme-dependencies.php');
require_once('lib/cpt.php');


/*
 * adds all meta information to the <head> element for us.
 */

function lionlab_meta() { ?>

  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="/apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-M38QL5P');
  </script>

<?php }

add_action('wp_head', 'lionlab_meta');


function lionlab_scripts() {

  $site_url = site_url();

  //change to empty string for concatenation or add .min for prod
  $path = '.min'; 

  if (strpos($site_url, 'localhost') !== false) {
    $path = '.min';
  }

  // Register main styles and scripts
  wp_enqueue_style( 'lionlab-style', get_template_directory_uri() . '/assets/css/main' . $path . '.css', array(), wp_get_theme()->get( 'Version' ) );

  wp_enqueue_script( 'lionlab-scripts', get_theme_file_uri( '/assets/js/main' . $path . '.js' ), array(), wp_get_theme()->get( 'Version' ), true );
}

add_action( 'wp_enqueue_scripts', 'lionlab_scripts' );


/**
 * Add fonts to header for preload option insted of importing through css in variables
 */
function lionlab_load_fonts() { ?>
  <link rel="preload" as="font" crossorigin="anonymous" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/CircularXXWeb-Book.woff" type="font/woff"> 
  <link rel="preload" as="font" crossorigin="anonymous" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/CircularXXWeb-Black.woff2" type="font/woff2">  
<?php }

add_action('wp_head', 'lionlab_load_fonts');


//defer JS
function defer_parsing_of_js( $url ) {
    if ( is_user_logged_in() ) return $url; //don't break WP Admin
    if ( FALSE === strpos( $url, '.js' ) ) return $url;
    return str_replace( ' src', ' defer src', $url );
}

add_filter( 'script_loader_tag', 'defer_parsing_of_js', 10 );



/* Enable ACF Options Pages */


if( function_exists('acf_add_options_page') ) {
  
  acf_add_options_page(array(
    'page_title'  => 'Globalt indhold',
    'menu_title'  => 'Globalt indhold',
    'menu_slug'   => 'global-content',
    'capability'  => 'edit_posts',
    'redirect'    => false
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Header',
    'menu_title'  => 'Header',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
    'page_title'  => 'Sidebar',
    'menu_title'  => 'Sidebar',
    'parent_slug' => 'global-content',
  ));
  
  acf_add_options_sub_page(array(
    'page_title'  => 'Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'global-content',
  ));

  acf_add_options_sub_page(array(
      'page_title'      => 'Cases Arkiv indstillinger',
      'parent_slug'     => 'edit.php?post_type=case', 
      'capability' => 'manage_options' 
  ));
}

/* Enable Featured Image */

add_theme_support( 'post-thumbnails' );

// Image sizes
add_image_size('featured_img', 1920, 745, true);
add_image_size('employee', 800, 954, true);
add_image_size('emoji', 480, 480, true);
add_image_size('references', 320, 180, true);

// Add created images sizes to dropdown in WP control panel
add_filter( 'image_size_names_choose', 'custom_image_sizes' );

function custom_image_sizes( $sizes ) {
  return array_merge( $sizes, array( 
    
  ) );
} 

/* Enable Custom Menus */

add_theme_support( 'menus' );

register_nav_menus(
  array(
    'lionlab-main-nav' => __( 'Main Nav', 'lionlab' )   // main nav in header
  )
);

function lionlab_main_nav() {
  // display the wp3 menu if available
  wp_nav_menu(array(
    'container' => false, // remove nav container
    'container_class' => '', // class of container (should you choose to use it)
    'menu' => __( 'Main Nav', 'lionlab' ), // nav name
    'menu_class' => 'nav__menu nav__menu--main', // adding custom nav class
    'theme_location' => 'lionlab-main-nav', // where it's located in the theme
    'before' => '', // before the menu
    'after' => '', // after the menu
    'link_before' => '', // before each link
    'link_after' => '', // after each link
    'depth' => 0,    // fallback function
    'walker' => new Clean_Walker_Nav () //remove wordpress walker
  ));
} /* end lionlab main nav */

add_filter( 'big_image_size_threshold', '__return_false' );