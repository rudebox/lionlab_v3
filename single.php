<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <?php   
    //post bg
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 

    //blog meta
    $author = get_the_author(); 

    $author_id = get_the_author_meta('ID');
    $avatar = get_field('avatar', 'user_'. $author_id );
  ?>

  <?php if (is_single() && empty($thumb) ) : ?>
    <div class="page__img-container">
      <div class="page__img page__img--placeholder"></div>
      <a onclick="window.history.go(-1); return false;" class="btn--back"><?php echo file_get_contents(get_template_directory_uri() . '/assets/img/caret-left-solid.svg'); ?> <span>tilbage</span></a>
    </div>
  <?php endif; ?>

  <div class="single__meta-container wrap hpad">
    <div class="single__meta flex">
        
        <div class="single__meta-wrap flex">
          <img class="single__avatar" src="<?php echo esc_attr($avatar['sizes']['thumbnail']); ?>" alt="<?php echo esc_html($author); ?>">

          <div class="single__meta-wrap">
            <h6 class="single__name"><?php echo esc_html($author); ?></h6>

            <?php if (get_the_author_meta('description') ) : ?>
              <span class="single__position yellow"><?php echo (get_the_author_meta('description') ); ?></span>
            <?php endif; ?>
          </div>

        </div>

    </div>

  </div>


  <section class="single padding--bottom">
    <div class="wrap hpad">
      <div class="row">

        <article class="single__content" itemscope itemtype="http://schema.org/BlogPosting">

          <div itemprop="articleBody">
            <?php the_content(); ?>
          </div>

        </article>

      </div>
    </div>
  </section>

  <?php get_template_part('parts/newsletter'); ?>

</main>

<?php get_template_part('parts/footer'); ?>