<?php 
/**
* Description: Lionlab contact field group options page
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


$img = get_field('form_img', 'options');
$title = get_field('form_title', 'options');
$text = get_field('form_text', 'options');
$form_id = get_field('form_id', 'options');
?>

<?php if (is_front_page() ) : ?>
<section class="contact--img padding--both">
	<div class="contact__container">
		<div loading="lazy" class="contact__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></div>
	</div>
</section>
<?php endif; ?>

<?php if (is_front_page() ) : ?>
<section class="contact padding--bottom bg--grey">
<?php else: ?>
<section class="contact padding--bottom">
<?php endif; ?>
	<div class="contact__container">
		
		<div class="contact__col bg--grey">
			<h2 class="contact__title"><?php echo esc_html($title); ?></h2>
			<?php echo $text; ?>
		</div>

	</div>
</section>