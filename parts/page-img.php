<?php   
  //post bg
  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 

  //field
  $service = get_field('case_service');
  $date = get_field('case_date');
  $website = get_field('case_website');
?>

<div class="page__img-container">

  <div class="page__img" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></div>

  <?php if (is_single() ) : ?>
    <a onclick="window.history.go(-1); return false;" class="btn--back"><?php echo file_get_contents(get_template_directory_uri() . '/assets/img/caret-left-solid.svg'); ?> <span>tilbage</span></a>
  <?php endif; ?>

  <?php if (is_singular('case') ) : ?>
    <div class="single-case__meta">

        <div class="single-case__meta-item">
          <p class="single-case__meta-desc">Kunde</p><br>
          <p class="single-case__meta-text"><?php echo the_title(); ?></p>
        </div>

        <div class="single-case__meta-item">
          <p class="single-case__meta-desc">Service</p><br>
          <p class="single-case__meta-text"><?php echo esc_html($service); ?></p>
        </div>

        <div class="single-case__meta-item">
          <p class="single-case__meta-desc">Periode</p><br>
          <p class="single-case__meta-text"><?php echo esc_html($date); ?></p>
        </div>

        <div class="single-case__meta-item">
          <p class="single-case__meta-desc">Website</p><br>
          <a target="blank" rel="noopener" href="<?php echo esc_url($website); ?>"><?php echo $website; ?></a>
        </div>

    </div>
  <?php endif; ?>

</div>