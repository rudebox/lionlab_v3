<?php 
	//contact informations
	$phone 	   = get_field('phone', 'options');
	$mail  	   = get_field('mail', 'options');
	$cvr   	   = get_field('cvr', 'options');
	$address   = get_field('address', 'options');

	//social links
	$fb = get_field('fb', 'options');
	$ig = get_field('ig', 'options');
	$li = get_field('li', 'options');
 ?>

<footer class="footer bg--black" itemscope itemtype="http://schema.org/WPFooter">
	<div class="footer__container">
		<div class="footer__row">

			<div class="footer__column footer__column--contact">
				<h3 class="footer__title">Kontakt</h3>
				<a href="mailto:<?php echo esc_attr($mail); ?>"><?php echo esc_html($mail); ?></a><br>
				<a href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a>
				<br>
				<p>CVR: <?php echo esc_html($cvr); ?></p>
				<?php echo $address; ?>
			</div>

			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="footer__column">
			 	<?php if ($title) : ?>
			 	<h3 class="footer__title"><?php echo esc_html($title); ?></h3>
			 	<?php endif; ?>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>

			<div class="footer__column footer__column--social">
				<div class="footer__scoial-links">
					<h3 class="footer__title">Social</h3>
					<a rel="noopener" target="_blank" href="<?php echo esc_url($fb); ?>">Facebook</a></br>
					<a rel="noopener" target="_blank" href="<?php echo esc_url($ig); ?>">Instagram</a></br>
					<a rel="noopener" target="_blank" href="<?php echo esc_url($li); ?>">LinkedIn</a>
				</div>

				<div class="footer__copyright">
					@ <?php echo gmdate('Y'); ?> Lionlab ApS 
				</div>
			</div>

		</div>
	</div>
</footer>
</div>
<?php get_template_part('parts/modal'); ?>
<div class="overlay"></div>

<?php wp_footer(); ?>

</body>
</html>