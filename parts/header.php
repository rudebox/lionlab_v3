<!DOCTYPE html>
<?php if (is_front_page() ) : ?>
<html <?php language_attributes(); ?> style="opacity: 0;">
<?php else: ?>
<html <?php language_attributes(); ?>>
<?php endif; ?>
<head> 
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <title><?php wp_title( '|', true, 'right' ); ?></title>

  <?php wp_head(); ?>
</head>

<?php 
  $theme = get_field('page_bg');
?>


<?php if ($theme === 'none') : ?>
<body <?php body_class() ?> itemscope itemtype="https://schema.org/WebPage" data-app="wrapper">
<?php else: ?>
<body <?php body_class('theme--' . $theme)?> itemscope itemtype="https://schema.org/WebPage" data-app="wrapper">
<?php endif; ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M38QL5P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<div class="transition__curtain"></div>

<header class="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="header__container">

    <a class="header__logo" aria-label="logo" href="<?php bloginfo('url'); ?>">
      <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/logo.svg'); ?>  
    </a>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <?php lionlab_main_nav(); ?>
      </div>
    </nav>

    <ul class="nav__menu">

      <li class="nav__item nav__notifications">
        <a class="nav__dropdown-trigger">
          <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/bell-solid.svg'); ?>
          <span class="blog__notifications-count"></span>
        </a>
        <ul class="nav__dropdown">  
          <h5 class="blog__notifications-header">Blog nyheder</h5>
            <?php 
              $i = 0;

              $args_query = array(
                'posts_per_page' => 3,
                'order' => 'DESC',
                'cat' => 2,
              );

              $query = new WP_Query( $args_query ); 
            ?>


            <?php 
              if ($query->have_posts() ): while ($query->have_posts() ): $query->the_post(); 

              $i++;

              //post img
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' ); 

              $title = get_the_title();
              $trim_title = wp_trim_words($title, 5, '...' );
            ?>

              <a class="blog__notifications" href="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/BlogPosting">

                <header class="blog__notifications-content">
                  <?php if ($thumb) : ?>
                    <img loading="lazy" class="blog__notifications-thumb" src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo esc_url($thumb['alt']); ?>" width="150" height="150">
                  <?php else : ?>
                    <div class="blog__notifications-thumb blog__notifications-thumb--placeholder"></div>
                  <?php endif; ?>

                  <div class="blog__notfications-wrap">
                    <h6 class="blog__notifications-title" itemprop="headline" title="<?php the_title_attribute(); ?>">
                      <?php echo $trim_title; ?>
                    </h6>
                    <span class="blog__notifications-meta"><time datetime="<?php the_time('c'); ?>" itemprop="datePublished"><?php the_time('d/m/Y'); ?></time></span>
                  </div>
                </header>

              </a>

            <?php endwhile; endif; ?>

            <?php wp_reset_postdata();  ?>

            <a class="blog__notifications-btn" href="/blog/">Gå til blog</a>
        </ul>
      </li>

      <li class="nav__modal-trigger nav__item">
        <button class="">
          <span>Gratis analyse</span>
          <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/facebook-messenger-brands.svg'); ?>
        </button>
      </li>
    </ul>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

  </div>
</header>

<div class="transition__container" data-app="container"> 