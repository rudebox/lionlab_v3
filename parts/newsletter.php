<?php 
	$title = get_field('newsletter_title', 'options');
	$text = get_field('newsletter_text', 'options');
 ?>

<section class="newsletter">
	<div class="newsletter__container">
		<div class="newsletter__col">

			<h2 class="newsletter__title"><?php echo esc_html($title); ?></h2>
			<?php echo $text; ?>

			<form action="https://lionlab.us18.list-manage.com/subscribe/post?u=0ed04b9902d368430d5882ce3&id=a04b25c056" method="POST" target="_blank">
		        
		        <div class="newsletter__form-field">
					 <div class="newsletter__form-wrap">
				     	<input class="newsletter__form-input" type="email" placeholder="Din e-mail adresse" autocapitalize="off" autocomplete="off" autocorrect="off" name="MERGE0" id="MERGE0" size="25" value="" required><br>
					    <input type="submit" class="newsletter__form-submit btn btn--wide btn--green" value="Tilmeld" name="submit">				  
				     </div>            
		        </div>
				
				<input type="hidden" name="mc_signupsource" value="hosted">
			</form>
		</div>
	</div>
</section>