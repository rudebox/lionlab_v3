<div class="modal">
	<div class="modal__container">
		<div class="modal__close"><?php echo file_get_contents(get_template_directory_uri() . '/assets/img/close.svg'); ?>  
		</div>

		<form id="form" cf-form>
			<cf-robot-message cf-questions="Hej med dig 👋<br>Godt at se dig."></cf-robot-message>
			<cf-robot-message cf-questions="Den digitale kunderejse kan være lang og snørklet, så det er ikke sikkert, du skal lægge alle dine æg i én kurv. Men ud fra vores analyse, kan vi i hvert fald råde dig til, hvilken kurv, der bør have flest æg.<br><br>Udfyld nedenstående informationer, så kigger vi på det allerede i dag."></cf-robot-message>
			<label for="firstname">
				<input type="text" cf-questions="Hvad må jeg kalde dig? 😊" cf-input-placeholder="Indtast dit navn" id="firstname" name="name" rquired />
			</label>

  			<fieldset cf-questions="Rart at møde dig, {firstname}. Hvad vil du gerne høre mere om?">
  				<label for="googleads"> 
			    	<input type="radio" id="googleads" value="Google Ads" name="services" cf-label="Google Ads" /><br>
				</label>
				<label for="seo">
			    	<input type="radio" id="seo" value="SEO" name="services" cf-label="SEO" /><br>
				</label>
				<label for="html5banner">
			    	<input type="radio" id="html5banner" value="HTML5 Banner"  name="services" cf-label="HTML5 Banner" /><br>
				</label>
				<label for="hjemmeside">
			    	<input type="radio" id="hjemmeside" value="Hjemmeside" name="services" cf-label="Hjemmeside" /><br>
				</label>
				<label for="some">
			    	<input type="radio" id="some" value="SoMe" name="services" cf-label="SoMe annoncering" /><br>
				</label>
				<label for="emailmarketing">
			    	<input type="radio" id="emailmarketing" value="E-mail marketing" name="services" cf-label="E-mail marketing" /><br>
				</label>
  			</fieldset>
  			<label for="email">
     			<input id="email" cf-error="E-mail adresse er ikke valid" type="email" cf-questions="Hvilken e-mail adresse kan vi fange dig på?" cf-input-placeholder="Indtast din e-mail adresse" name="email" rquired />
     		</label>
     		<label for="url">
	        	<input id="url" type="url" cf-questions="Hvad er din hjemmesides URL?" cf-input-placeholder="Indtast din hjemmeside URL" name="website" rquired />
	    	</label>
		</form>
		<div id="cf-context" cf-context></div>
	</div>
</div>