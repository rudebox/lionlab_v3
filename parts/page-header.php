<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//pageheader variables
	$page_img = get_field('page_img');
	$page_bg = get_field('page_bg');


	//case archive title
	$case_title = get_field('case_archive_title', 'options');	

	//post bg
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 

	//check if is has thumb or is template
	if (is_page_template('parts/contact-template.php') || is_page_template('page-layouts.php') || is_singular('case') || !is_home() ) {
		$class = 'page__hero--intersect';
	} else {
		$class = '';
	}	
?>

<?php if (!is_front_page() && !is_post_type_archive() ) : ?> 
<section class="page__hero <?php echo esc_attr($class); ?> bg--<?php echo esc_attr($page_bg); ?>">
<?php else : ?>
<section class="page__hero bg--<?php echo esc_attr($page_bg); ?>">
<?php endif; ?>
	<div class="page__container">
		<div class="row">

			<div class="page__col">
				<h1 class="page__title">
					<?php if (!is_front_page() && !is_singular('post') && !is_home() && !is_post_type_archive() ) : ?>				
					<span class="page__slug"><?php echo get_the_title($post->post_parent); ?></span>					
					<?php endif; ?>
					<?php if (is_home() ) : ?>
						<span class="h3 yellow">Blog</span>
					<?php endif; ?>
					<?php if (is_post_type_archive() ) : ?>
						<span class="h3 yellow">Cases</span>
					<?php endif; ?>
					<?php if (is_post_type_archive() ) : ?>
						<?php echo $case_title; ?> 
					<?php else : ?>
						<?php echo $title; ?> 
					<?php endif; ?>
				</h1>
				<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/lion.svg'); ?>

			</div>

		</div>
	</div>
</section>

<?php if ($thumb && !is_home() && !is_post_type_archive() ) : ?>
	<?php get_template_part('parts/page', 'img');?>
<?php endif; ?>