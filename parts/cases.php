<?php 
  //sections settings
  $margin = get_sub_field('margin');
?>

 <section class="cases padding--both">

    <div class="cases__container">
      <div class="cases__row">
        
          <?php 

            $cases = get_field('cases_pick');

            //query arguments
            $args = array(
              'posts_per_page' => 2,
              'post_type' => 'case',
              'post__in' => $cases
            );
             
            $query = new WP_QUERY($args);
            
           ?>

          <?php if ($query->have_posts()): ?>
            <?php while ($query->have_posts()): $query->the_post(); ?>


            <?php   
              //get thumb
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

              //get categories
              $categories = get_the_category();

              $title = get_field('page_title');
            ?>

             <a loading="lazy" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="cases__item col-sm-6" itemscope itemtype="http://schema.org/BlogPosting" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)">
                
                <div class="cases__wrap flex flex--center flex--justify flex--wrap">
                  <header>
                    <h2 class="cases__title--meta yellow h4">                   
                        <?php the_title(); ?>
                    </h2>

                    <h2 itemprop="headline" class="cases__title h3"><?php echo esc_html($title); ?></h2>

                  </header>
                </div>

              </a>

            <?php endwhile; wp_reset_postdata(); else: ?>
              
              <p>Cases er ikke tilgængelige på nuværende tidspunkt.</p>

          <?php endif; ?>

      </div>
    </div>
  </section>