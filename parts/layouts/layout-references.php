<?php 
/**
* Description: Lionlab references repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

if (have_rows('reference') ) :
?>

<section class="references padding--<?php echo esc_attr($margin); ?>">
	<div class="references__container">

		<div class="references__row"> 
			<?php while (have_rows('reference') ) : the_row(); 
				$logo = get_sub_field('logo');
			?>

			<div class="references__item">				
				<img class="references__img" width="<?php echo esc_attr($logo['sizes']['references-width']); ?>" height="<?php echo esc_attr($logo['sizes']['references-height']); ?>" loading="lazy" src="<?php echo esc_url($logo['sizes']['references']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>">
			</div>
			<?php endwhile; ?>
		</div>

	</div>
</section>
<?php endif; ?>