<?php 
/**
* Description: Lionlab references repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

if (have_rows('partner') ) :
?>

<section class="partners padding--<?php echo esc_attr($margin); ?>">
	<div class="partners__container">

		<div class="partners__row"> 
			<?php while (have_rows('partner') ) : the_row(); 
				$logo = get_sub_field('logo');
			?>

			<div class="partners__item">				
				<img class="partners__img" loading="lazy" src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>">
			</div>
			<?php endwhile; ?>
		</div>

	</div>
</section>
<?php endif; ?>