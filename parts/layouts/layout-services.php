<?php 
/**
* Description: Lionlab services repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

//fields
$header = get_sub_field('service_header');
$text = get_sub_field('service_text');

?>

<section class="services padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">

		<div class="services__row">

			<div class="services__text bg--green">
				<h2 class="services__header"><?php echo $header; ?></h2>
				<?php echo $text; ?>
			</div>

			<div class="services__employees">
				<?php 
					if (have_rows('service_employees') ) : while (have_rows('service_employees') ) : the_row();	
					$img = get_sub_field('service_employee_img');
					$index = get_row_index();
				?>

					<img loading="lazy" class="services__employees-img" data-index="<?php echo esc_attr($index); ?>" src="<?php echo esc_url($img['sizes']['emoji']); ?>" alt="<?php echo esc_attr($img['alt']); ?>" width="<?php echo esc_attr($img['sizes']['emoji-width']); ?>" height="<?php echo esc_attr($img['sizes']['emoji-height']); ?>">

				<?php endwhile; endif; ?>
			</div>

			<div class="services__accordions bg--grey">
				<?php if (have_rows('service') ) : while (have_rows('service') ) : the_row(); 
					$name = get_sub_field('service_name');
					$text = get_sub_field('service_text');
					$link = get_sub_field('service_link');
					$link_text = get_sub_field('service_link_text');
					$index = get_row_index();
				?>

				<div class="accordion__item">
					<div class="accordion__wrapper">		
						<h4 class="accordion__title" data-index="<?php echo esc_attr($index); ?>"><?php echo esc_html($name); ?> <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/caret-up-solid.svg'); ?></h4>
						<div class="accordion__panel">
							<p class="accordion__text"><?php echo $text; ?></p>
							<?php if ($link) : ?>
							<a class="accordion__link" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<?php endwhile; endif; ?>
			</div>
		</div>

	</div>
</section>