<?php 
/**
* Description: Lionlab services repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');
$bg = get_sub_field('bg');

//fields
$header = get_sub_field('cta_header');
$text = get_sub_field('cta_text');
$img = get_sub_field('cta_employee_img');

//social links
$fb = get_field('fb', 'options');
$ig = get_field('ig', 'options');
$li = get_field('li', 'options');
?>

<section class="cta padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">

		<div class="cta__row">

			<?php if ($img) : ?>
			<div class="cta__text bg--<?php echo esc_attr($bg); ?>" >
			<?php else: ?>
			<div class="cta__text cta__text--contact bg--<?php echo esc_attr($bg); ?>" itemscope itemtype="http://schema.org/LocalBusiness">
			<?php endif; ?>
				<h2 class="cta__header"><?php echo $header; ?></h2>
				<?php echo $text; ?>

				<?php if (is_page_template('parts/contact-template.php') ) : ?>
					<div class="cta__text-wrap">
						<a class="cta__text-link" href="<?php echo esc_url($fb); ?>" target="_blank">
							<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/facebook-f-brands.svg'); ?> 
						</a>

						<a class="cta__text-link" href="<?php echo esc_url($ig); ?>" target="_blank">
							<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/instagram-brands.svg'); ?> 
						</a>

						<a class="cta__text-link" href="<?php echo esc_url($li); ?>" target="_blank">
							<?php echo file_get_contents(get_template_directory_uri() . '/assets/img/linkedin-in-brands.svg'); ?> 
						</a>
					</div>
				<?php endif; ?>
			</div>

			<?php 
				if ($img) :
			?>

			<div class="cta__employees">
				<img class="cta__employees-img " src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
			</div>

			<?php endif; ?>

			<?php if ($img) : ?>
			<div class="cta__form bg--grey">
			<?php else : ?>
			<div class="cta__form cta__form--contact bg--grey">
			<?php endif; ?>
				<?php echo do_shortcode('[hf_form slug="kontakt"]'); ?>
			</div>
		</div>

	</div>
</section>