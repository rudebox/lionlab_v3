<?php 
  //section settings
  $margin = get_sub_field('margin');
  $bg = get_sub_field('bg');

  //video 
  $video_teaser = get_sub_field('video_teaser');
  $video_mp4 = get_sub_field('video_mp4');
  $video_ogv = get_sub_field('video_ogv');
  $video_webm = get_sub_field('video_webm');
  $video_yt = get_sub_field('video_yt');
 ?>

<section class="video padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
	<div class="wrap--fluid">
		<div class="row--fluid flex flex--hvalign">
			<div class="col-sm-12 flex flex--hvalign video__wrap">
				
				<video class="video__video video__video--preview" preload="auto" autoplay playsinline loop muted="muted" volume="0">
					<source src="<?php echo esc_url($video_teaser); ?>" type="video/mp4" codecs="avc1, mp4a">
				</video>
				
				<a class="video__controls" data-video="<?php echo esc_url($video_yt); ?>" data-video="<?php echo esc_url($video_yt); ?>">
			    	<div class="video__play"><?php echo file_get_contents(get_template_directory_uri() . '/assets/img/play.svg'); ?></div>
				</a>		
			
			</div>

		</div>
	</div>
</section>