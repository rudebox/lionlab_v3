<?php 
/**
* Description: Lionlab services repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

$text = get_sub_field('clients_text');
$header = get_sub_field('clients_header');

if (have_rows('clients_results') ) :
?>

<section class="client-results padding--<?php echo esc_attr($margin); ?>">
	<div class="client-results__container">

		<div class="client-results__row">

			<div class="client-results__intro">	
				<h2 class="client-results__header"><?php echo esc_html($header); ?></h2>
				<?php echo $text; ?>
			</div>

			<div class="client-results__results">
				<?php while (have_rows('clients_results') ) : the_row(); 
					$logo = get_sub_field('logo');
					$name = get_sub_field('name');
					$services = get_sub_field('services');
					$text = get_sub_field('text');
					$result = get_sub_field('result');
				?>

				<div class="client-results__item">	
	 				<div class="client-results__img">
						<img loading="lazy" width="150" height="150" src="<?php echo esc_url($logo['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>">
					</div>

					<h6 class="client-results__title"><?php echo esc_html($name); ?></h6>

					<p class="client-results__text"><?php echo esc_html($text); ?></p>

					<div class="client-results__result"><?php echo esc_html($result); ?> <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/polygon.svg'); ?></div>

				</div>
				<?php endwhile; ?>
			</div>
		</div>

	</div>
</section>
<?php endif; ?>