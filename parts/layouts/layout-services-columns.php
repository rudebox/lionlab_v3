<?php 
/**
* Description: Lionlab services repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');
$header = get_sub_field('header');

if (have_rows('service') ) :
?>

<section class="services-columns padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">

		<h2 class="services__header"><?php echo $header; ?></h2>

		<div class="services-columns__row row flex flex--wrap">	

			<?php while (have_rows('service') ) : the_row(); 
				$name = get_sub_field('service_name');
				$text = get_sub_field('service_text');
				$link = get_sub_field('service_link');
			?>

				<a href="<?php echo esc_url($link); ?>" class="services-columns__column">	
					
					<h4 class="services-columns__title"><?php echo esc_html($name); ?></h4>
					<?php
						 if (have_rows('service_consultants') ) : while (have_rows('service_consultants') ) : the_row(); 
						 $img = get_sub_field('img');
					?>
						<img class="services-columns__consultants" src="<?php echo esc_url($img['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>">
					<?php endwhile; endif; ?>

					<span class="services-columns__btn">Læs mere</span>		
				</a>
				<?php echo $text; ?>
			
			<?php endwhile; ?>

		</div>
	</div>
</section>
<?php endif; ?>