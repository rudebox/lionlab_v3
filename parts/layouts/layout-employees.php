<?php 
/**
* Description: Lionlab employees repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

if (have_rows('employee') ) :

if (is_page_template('parts/contact-template.php') ) {
	$class = 'employee--contact';
}
?>

<section class="employee <?php echo esc_attr($class); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="employee__container">

		<div class="employee__row">
			<?php while (have_rows('employee') ) : the_row(); 
				$name = get_sub_field('employee_name');
				$img = get_sub_field('employee_img');
				$video = get_sub_field('employee_video');
				$mail = get_sub_field('employee_mail');
				$phone = get_sub_field('employee_phone');
				$position = get_sub_field('employee_position');
				$linkedin = get_sub_field('employee_linkedin');
			?>

			<div class="employee__item">
				<div class="employee__wrap">
					<?php if ($video) : ?>
					<video autoplay loop playsinline preload="auto" loop muted="muted" volume="0">
						<source src="<?php echo esc_url($video); ?>" type="video/webm" codecs="vp8, vorbis">
					</video>
					<?php else: ?>
						<img loading="lazy" src="<?php echo esc_url($img['sizes']['employee']); ?>" alt="<?php echo esc_attr($img['alt']); ?>" width="<?php echo esc_attr($img['sizes']['employee-width']); ?>" height="<?php echo esc_attr($img['sizes']['employee-height']); ?>">
					<?php endif; ?>
					<?php if ($linkedin) : ?>
						<a rel="noopener" target="_blank" class="employee__linkedin" href="<?php echo esc_url($linkedin); ?>"><?php echo file_get_contents(get_template_directory_uri() . '/assets/img/linkedin-in-brands.svg'); ?></a>
					<?php endif; ?>
				</div>		
				<h5 class="employee__position"><?php echo $position; ?></h5>
				<h3 class="employee__name"><?php echo esc_html($name); ?></h3>
				<a class="employee__link" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
				<a class="employee__link" href="tel:<?php echo esc_html(get_formatted_phone($phone)); ?>"><?php echo esc_html($phone); ?></a>
				
			</div>
			<?php endwhile; ?>
		</div>

	</div>
</section>
<?php endif; ?>