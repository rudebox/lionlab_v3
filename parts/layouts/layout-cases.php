<?php 
  //sections settings
  $margin = get_sub_field('margin');
?>

 <section class="cases padding--<?php echo esc_attr($margin); ?>">

    <div class="wrap hpad">
      <div class="row flex flex--wrap masonry">
        
          <?php 
            //counter
            $i=0;

            $cases = get_sub_field('selected_case');

            //query arguments
            $args = array(
              'posts_per_page' => -1,
              'post_type' => 'case',
              'post__in' => $cases
            );
             
            $query = new WP_QUERY($args);
            
           ?>

          <?php if ($query->have_posts()): ?>
            <?php while ($query->have_posts()): $query->the_post(); ?>


            <?php   
              //get thumb
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

              //get categories
              $categories = get_the_category();

              $title = get_field('page_title');

              $reference_text = get_sub_field('selected_case_referene_text');
              $reference_rating = get_sub_field('selected_case_referene_rating');
              $reference_img = get_sub_field('selected_case_reference_img');
              $reference_name = get_sub_field('selected_case_reference_name');

              $svg_path = file_get_contents(get_template_directory_uri() . '/assets/img/star-solid.svg');

              if ($reference_rating === '1') {
                $reference_rating = $svg_path;
              } 

              if ($reference_rating === '2') {
                $reference_rating = $svg_path . $svg_path;
              } 

              if ($reference_rating === '3') {
                $reference_rating = $svg_path . $svg_path . $svg_path;
              } 

              if ($reference_rating === '4') {
                $reference_rating = $svg_path . $svg_path . $svg_path . $svg_path;
              } 

              if ($reference_rating === '5') {
                $reference_rating = $svg_path . $svg_path . $svg_path . $svg_path . $svg_path;
              } 

              $i++;

              //insert reference
              if ($i === 4 && $reference_text) {
                echo '<div class="cases__reference col-sm-6">';
                  echo '<div class="cases__reference-rating yellow">' . $reference_rating . '</div>';
                  echo '<p>' . $reference_text . '<p>';
                  echo '<div class="cases__reference-wrap">';
                  echo '<h4 class="cases__reference-name">' . $reference_name . '</h4>';
                    echo '<img loading="lazy" class="cases__reference-img" alt="' . $reference_img['alt'] . '" src="' . $reference_img['sizes']['thumbnail'] . '" width="150" height="150">';
                  echo '</div>';
                echo '</div>';
              }
            ?>

             <a loading="lazy" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="cases__item col-sm-6" itemscope itemtype="http://schema.org/BlogPosting" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)">
                
                <div class="cases__wrap flex flex--center flex--justify flex--wrap">
                  <header>
                    <h2 class="cases__title--meta yellow h4">                   
                        <?php the_title(); ?>
                    </h2>

                    <h2 itemprop="headline" class="cases__title h3"><?php echo esc_html($title); ?></h2>
                  </header>
                </div>

              </a>

            <?php endwhile; wp_reset_postdata(); else: ?>
              
              <p>Cases er ikke tilgængelige på nuværende tidspunkt.</p>

          <?php endif; ?>

      </div>
    </div>
  </section>